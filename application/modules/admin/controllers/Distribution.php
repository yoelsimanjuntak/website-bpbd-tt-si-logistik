<?php
class Distribution extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEPUSKESMAS) {
      redirect('admin/dashboard');
    }
  }

  public function index() {
    $data['title'] = "Distribusi";
    $this->template->load('main', 'admin/distribution/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdPuskesmas = !empty($_POST['idPuskesmas'])?$_POST['idPuskesmas']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_DATEDISTRIBUTION=>'desc');
    $orderables = array(null,COL_DATEDISTRIBUTION,COL_NMPUSKESMAS,COL_NMREFERENSI,null,null,COL_CREATEDON);
    $cols = array(COL_DATEDISTRIBUTION,COL_NMPUSKESMAS,COL_NMREFERENSI,COL_CREATEDBY);

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKDISTRIBUTION.".".COL_CREATEDBY,"left")
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = ".TBL_TSTOCKDISTRIBUTION.".".COL_IDPUSKESMAS,"left")
    ->get(TBL_TSTOCKDISTRIBUTION);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMPUSKESMAS) $item = 'pus.'.COL_NMPUSKESMAS;
      if($item == COL_CREATEDBY) $item = TBL_TSTOCKDISTRIBUTION.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_DATEDISTRIBUTION.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_DATEDISTRIBUTION.' <= ', $dateTo);
    }
    if(!empty($IdPuskesmas)) {
      $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_IDPUSKESMAS, $IdPuskesmas);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tstockdistribution.*, pus.NmPuskesmas, uc.Nm_FullName as Nm_CreatedBy, (select count(*) from tstockdistribution_items it where it.IdDistribution = tstockdistribution.Uniq) as NumItems')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKDISTRIBUTION.".".COL_CREATEDBY,"left")
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = ".TBL_TSTOCKDISTRIBUTION.".".COL_IDPUSKESMAS,"left")
    ->order_by(TBL_TSTOCKDISTRIBUTION.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TSTOCKDISTRIBUTION, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      if($ruser[COL_ROLEID] != ROLEPUSKESMAS) {
        $htmlBtn .= '<a href="'.site_url('admin/distribution/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i></a>&nbsp;';
      }
      $htmlBtn .= '<a href="'.site_url('admin/distribution/view/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-info btn-view"><i class="fas fa-info-circle"></i></a>';
      $data[] = array(
        $htmlBtn,
        date('Y-m-d', strtotime($r[COL_DATEDISTRIBUTION])),
        $r[COL_NMPUSKESMAS],
        $r[COL_NMREFERENSI],
        number_format($r['NumItems']),
        $r[COL_CREATEDBY],
        date('Y-m-d H:i:s', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] == ROLEPUSKESMAS) {
      ShowJsonError('Anda tidak memiliki otorisasi.');
      return;
    }
    if(!empty($_POST)) {
      $arrDet = array();
      $det = $this->input->post("DistItems");
      $data = array(
        COL_IDPUSKESMAS => $this->input->post(COL_IDPUSKESMAS),
        COL_NMREFERENSI => $this->input->post(COL_NMREFERENSI),
        COL_DATEDISTRIBUTION => $this->input->post(COL_DATEDISTRIBUTION),
        COL_NMREMARKS => $this->input->post(COL_NMREMARKS),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TSTOCKDISTRIBUTION, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idDist = $this->db->insert_id();
        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {
            /* CHECK STOCK */
            $qCheck = @"
            select *, Jumlah - coalesce((select sum(i.Jumlah) from tstockdistribution_items i where i.`IdReceipt` = r.`Uniq`),0) as JlhSisa
          	from tstockreceipt r
          	where
          		r.Uniq = ?
              and r.DateReceipt <= ?
            ";
            $rcheck = $this->db->query($qCheck, array($s->IdReceipt, $data[COL_DATEDISTRIBUTION]))->row_array();
            if(empty($rcheck)) {
              throw new Exception('No. BATCH tidak valid. Silakan periksa kembali.');
            }
            if($rcheck['JlhSisa'] < $s->Jumlah) {
              throw new Exception('Jumlah stok pada item BATCH No. <b>'.$rcheck[COL_NMBATCH].'</b> tidak mencukupi.<br />SISA STOK: <b>'.number_format($rcheck['JlhSisa']).'</b>');
            }
            /* CHECK STOCK */

            $arrDet[] = array(
              COL_IDDISTRIBUTION=>$idDist,
              COL_IDRECEIPT=>$s->IdReceipt,
              COL_JUMLAH=>toNum($s->Jumlah)
            );
          }
        }

        if(!empty($arrDet)) {
          $res = $this->db->insert_batch(TBL_TSTOCKDISTRIBUTION_ITEMS, $arrDet);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        } else {
          throw new Exception('Item tidak boleh kosong.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      /*ShowJsonError('Parameter tidak valid.');
      return;*/
      $data['DistItems'] = json_encode(array());
      $this->load->view('admin/distribution/form', $data);
    }
  }

  public function view($id) {
    $rdata = $this->db
    ->select('tstockdistribution.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKDISTRIBUTION.".".COL_CREATEDBY,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_TSTOCKDISTRIBUTION)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tstockdistribution_items.*, st.NmStock, st.NmSatuan, r.NmBatch')
    ->join(TBL_TSTOCKRECEIPT.' r','r.'.COL_UNIQ." = ".TBL_TSTOCKDISTRIBUTION_ITEMS.".".COL_IDRECEIPT,"left")
    ->join(TBL_MSTOCK.' st','st.'.COL_IDSTOCK." = r.".COL_IDSTOCK,"left")
    ->where(COL_IDDISTRIBUTION, $rdata[COL_UNIQ])
    ->get(TBL_TSTOCKDISTRIBUTION_ITEMS)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdReceipt'=> $det[COL_IDRECEIPT],
        'NmBatch'=> $det[COL_NMBATCH],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'Jumlah'=>$det[COL_JUMLAH]
      );
    }
    $this->load->view('admin/distribution/form', array('DistItems'=>json_encode($arrdet),'data'=>$rdata,'disabled'=>true));
  }

  public function delete($id) {
    if($ruser[COL_ROLEID] == ROLEPUSKESMAS) {
      ShowJsonError('Anda tidak memiliki otorisasi.');
      return;
    }
    $rissue = $this->db
    ->where("IdItem in (select i.Uniq from tstockdistribution_items i where i.IdDistribution = $id)", null, false)
    ->get(TBL_TSTOCKISSUE)
    ->row_array();
    if(!empty($rissue)) {
      ShowJsonError('Data tidak dapat dihapus karena sudah memiliki relasi dengan data PEMAKAIAN.');
      return;
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TSTOCKDISTRIBUTION);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    return;
  }
}
?>
